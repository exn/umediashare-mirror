# uMediaShare 

[![Join the chat at https://gitter.im/linuxenko/umediashare](https://badges.gitter.im/linuxenko/umediashare.svg)](https://gitter.im/linuxenko/umediashare?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) [![Build Status](https://travis-ci.org/linuxenko/umediashare.svg?branch=master)](https://travis-ci.org/linuxenko/umediashare) [![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/linuxenko/umediashare/trend.png)](https://bitdeli.com/free "Bitdeli Badge")  

### About

Media content sharing between dlna/upnp devices.<br />
Currently supported : <br />
 * Android devices (CastOn free application from store) <br />
 * LG TV <br />
 * Linux devices and desktops (gmediarenderer , opengl renderer application)

Application based on [ember-cli electron](https://github.com/felixrieseberg/ember-electron)

## Screenshots
<a href="https://raw.githubusercontent.com/linuxenko/umediashare/media/images/dashboard.png">
<img height=360 width=550 src="https://raw.githubusercontent.com/linuxenko/umediashare/media/thumbs/dashboard-thumb.png" align=left />
</a>
[![](https://raw.githubusercontent.com/linuxenko/umediashare/media/thumbs/deviceinfo-p.png)](https://raw.githubusercontent.com/linuxenko/umediashare/media/images/deviceinfo.png)   [![](https://raw.githubusercontent.com/linuxenko/umediashare/media/thumbs/devices-p.png)](https://raw.githubusercontent.com/linuxenko/umediashare/media/images/devices.png)  

[More...](https://github.com/linuxenko/umediashare/tree/media/images)

## Installation

```
$ npm install -g ember-cli
$ npm install
$ bower install
```

Run :

```
$ ember electron
```

## Download 

  You can download following binaries from [binary project builds v0.0.1](https://github.com/linuxenko/umediashare/releases/tag/v0.0.1)
  * [ums-darwin-x64.tgz](https://github.com/linuxenko/umediashare/releases/download/v0.0.1/ums-darwin-x64.tgz) <br />
  * [ums-linux-ia32.tgz](https://github.com/linuxenko/umediashare/releases/download/v0.0.1/ums-linux-ia32.tgz) <br />
  * [ums-linux-x64.tgz](https://github.com/linuxenko/umediashare/releases/download/v0.0.1/ums-linux-x64.tgz) <br />
  * [ums-win32-ia32.tgz](https://github.com/linuxenko/umediashare/releases/download/v0.0.1/ums-win32-ia32.tgz) <br />
  * [ums-win32-x64.tgz](https://github.com/linuxenko/umediashare/releases/download/v0.0.1/ums-win32-x64.tgz)


### Technologies: <br />
 * ssdp discovery<br />
 * upnp protocol <br />
 * ember-cli (ember-elecron) as development framework<br />
 * bootstrap for styling<br />
 * electron application packaging<br />
 
 
### TODO:<br />
  * Bug fixing <br />
  * Fixing bugs <br />
  * ... ok ?
 
## Contribution free

 * Code, issues, comment, questions .. 


### Old promo
Demo http://youtu.be/1xI6HVA9oGk<br />
[Test project for development in team](https://github.com/linuxenko/umediashare/blob/master/PROMO.md). Development is in progress<br />



